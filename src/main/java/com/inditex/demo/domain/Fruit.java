/**
 * Fruit.java 08-may-2020
 *
 * Copyright 2020 INDITEX.
 * Systems Department
 */
package com.inditex.demo.domain;

import org.bson.types.ObjectId;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.quarkus.mongodb.panache.reactive.ReactivePanacheMongoEntityBase;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@RegisterForReflection
@Schema(name="Fruit", description = "Description of the Fruit")
public class Fruit extends ReactivePanacheMongoEntityBase {

    @Schema(hidden = true)
    @JsonIgnore
    public ObjectId id;

    @Schema(required =  true, defaultValue = "Pear")
    public String name;

    @Schema(required =  true, defaultValue = "Winter Fruit")
    public String description;

}
