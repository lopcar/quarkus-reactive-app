/**
 * MemoryData.java 25-may-2020
 *
 * Copyright 2020 INDITEX.
 * Systems Department
 */
package com.inditex.demo.domain;

import lombok.Data;

@Data
public class MemoryData {

    private String maxMemory;
    private String allocatedMemory;
    private String totalFreeMemory;
    private String freeMemory;
    
    
}
