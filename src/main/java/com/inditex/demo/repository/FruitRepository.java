/**
 * FruitRepository.java 08-may-2020
 *
 * Copyright 2020 INDITEX.
 * Systems Department
 */
package com.inditex.demo.repository;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import com.inditex.demo.domain.Fruit;

import io.quarkus.mongodb.panache.reactive.ReactivePanacheMongoRepository;
import io.quarkus.panache.common.Sort;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;

@ApplicationScoped
public class FruitRepository implements ReactivePanacheMongoRepository<Fruit> {

    public Uni<Fruit> findByName(final String name) {
        return this.find("name", name).firstResult();
    }

    @Override
    public Uni<List<Fruit>> listAll() {
        return this.listAll(Sort.by("name"));
    }

    public Multi<Fruit> getAll() {
        return this.streamAll();
    }

    public Uni<Long> deleteByName(final String name) {
        return this.delete("name", name);
    }
}
