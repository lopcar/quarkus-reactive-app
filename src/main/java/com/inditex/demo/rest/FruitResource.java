/**
 * FruitResource.java 08-may-2020
 *
 * Copyright 2020 INDITEX.
 * Systems Department
 */
package com.inditex.demo.rest;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;

import com.inditex.demo.domain.Fruit;
import com.inditex.demo.domain.MemoryData;
import com.inditex.demo.service.FruitService;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;

/**
 * The Class FruitResource.
 *
 * @author capgemini
 */
@Path("/fruits")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class FruitResource {

    private static final Logger LOGGER = Logger.getLogger("Main");

    /** The fruit service. */
    @Inject
    FruitService fruitService;

    /**
     * Find fruits.
     *
     * @return the uni
     */
    @GET
    public Uni<List<Fruit>> findFruits() {
        LOGGER.info("findFruits");
        return this.fruitService.list();
    }

    /**
     * Find all fruits.
     *
     * @return the multi
     */
    @GET
    @Path("/findAll")
    public Multi<Fruit> findAllFruits() {
        LOGGER.info("findFruits");
        return this.fruitService.getAll();
    }

    /**
     * Find by name.
     *
     * @param name
     *            name
     * @return the uni
     */
    @GET
    @Path("/{name}")
    public Uni<Fruit> findByName(@PathParam(value = "name") final String name) {
        LOGGER.info("findByName: " + name);
        return this.fruitService.findByName(name);
    }

    /**
     * Create the.
     *
     * @param fruit
     *            fruit
     * @return the uni
     */
    @POST
    public Uni<Void> create(final Fruit fruit) {
        LOGGER.info("create:" + fruit.getName() + " " + fruit.getDescription());
        return this.fruitService.add(fruit);
    }

    /**
     * Delete.
     *
     * @param name
     *            name
     * @return the uni
     */
    @DELETE
    public Uni<Long> delete(final String name) {
        LOGGER.info("delete: " + name);
        return this.fruitService.deleteByName(name);
    }

    /**
     * Delete all.
     *
     * @return the uni
     */
    @DELETE
    @Path("/all")
    public Uni<Long> deleteAll() {
        LOGGER.info("deleteAll");
        return this.fruitService.deleteAll();
    }

    /**
     * Count.
     *
     * @return the uni
     */
    @GET
    @Path("/count")
    public Uni<Long> count() {
        LOGGER.info("count");
        return this.fruitService.count();
    }

    /**
     * Memory check.
     *
     * @return the memory data
     */
    @GET
    @Path("/memoryCheck")
    public MemoryData memoryCheck() {
        LOGGER.info("memoryCheck");
        return this.fruitService.memoryCheck();
    }
}