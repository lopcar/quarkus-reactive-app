/**
 * FruitService.java 08-may-2020
 *
 * Copyright 2020 INDITEX.
 * Systems Department
 */
package com.inditex.demo.service;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.inditex.demo.domain.Fruit;
import com.inditex.demo.domain.MemoryData;
import com.inditex.demo.repository.FruitRepository;
import com.inditex.demo.utils.MemoryUtils;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;

/**
 * The Class FruitService.
 *
 * @author capgemini
 */
@ApplicationScoped
public class FruitService {

    /** The fruit repository. */
    @Inject
    FruitRepository fruitRepository;

    /**
     * List.
     *
     * @return the uni
     */
    public Uni<List<Fruit>> list() {
        return this.fruitRepository.listAll();
    }

    /**
     * Find by name.
     *
     * @param name
     *            name
     * @return the uni
     */
    public Uni<Fruit> findByName(final String name) {
        return this.fruitRepository.findByName(name);
    }

    /**
     * Adds the.
     *
     * @param fruit
     *            fruit
     */
    public Uni<Void> add(final Fruit fruit) {
        return this.fruitRepository.persist(fruit);
    }

    /**
     * Delete by name.
     *
     * @param name
     *            name
     * @return the uni
     */
    public Uni<Long> deleteByName(final String name) {
        return this.fruitRepository.delete(name);
    }

    /**
     * Delete all.
     *
     * @return the uni
     */
    public Uni<Long> deleteAll() {
        return this.fruitRepository.deleteAll();
    }

    /**
     * Count.
     *
     * @return the uni
     */
    public Uni<Long> count() {
        return this.fruitRepository.count();
    }

    /**
     * Get all.
     *
     * @return all
     */
    public Multi<Fruit> getAll() {
        return this.fruitRepository.getAll();
    }

    /**
     * Memory check.
     *
     * @return the memory data
     */
    public MemoryData memoryCheck() {
        return MemoryUtils.checkMemory();
    }

}
