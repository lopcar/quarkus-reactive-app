/**
 * FruitResourceTest.java 11-may-2020
 *
 * Copyright 2020 INDITEX.
 * Systems Department
 */
package com.inditex.demo.rest;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;

import java.util.Date;

import javax.ws.rs.core.MediaType;

import org.bson.types.ObjectId;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.inditex.demo.domain.Fruit;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class FruitResourceTest {

    private final Fruit apple = new Fruit(new ObjectId(new Date()), "Apple", "Winter fruit");
    private final Fruit pear = new Fruit(new ObjectId(new Date()), "Pear", "Winter fruit");

    @BeforeEach
    public void initialize() {
        // Create Fruit
        given().body(this.apple).header("Content-Type", MediaType.APPLICATION_JSON).when().post("/fruits/").then()
        .statusCode(200);
    }

    @AfterEach
    public void destroy() {
        // Delete Fruit
        given().body(this.apple).header("Content-Type", MediaType.APPLICATION_JSON).when().delete("/fruits").then()
        .statusCode(200);
    }

    @Test
    public void testFindFruits() {
        given()
        .when().get("/fruits")
        .then()
        .statusCode(200)
        .body("$.size()", is(1),
                "name", containsInAnyOrder(this.apple.getName()),
                "description", containsInAnyOrder(this.apple.getDescription()));
    }

    @Test
    public void testFindByName() {
        given()
        .when().get("/fruits/" + this.apple.getName())
        .then()
        .statusCode(200)
        .body("name", equalTo(this.apple.getName()), "description", equalTo(this.apple.getDescription()));
    }

    @Test
    public void testCreate() {

        // Create Fruit
        given()
        .body(this.pear)
        .header("Content-Type", MediaType.APPLICATION_JSON)
        .when()
        .post("/fruits/")
        .then()
        .statusCode(200)
        .body("$.size()", is(2), "name", containsInAnyOrder(this.pear.getName(), this.apple.getName()),
                "description", containsInAnyOrder(this.pear.getDescription(), this.apple.getDescription()));

        // Delete Fruit
        given()
        .body(this.pear)
        .header("Content-Type", MediaType.APPLICATION_JSON)
        .when()
        .delete("/fruits")
        .then()
        .statusCode(200);

        // Find fruit
        given()
        .when().get("/fruits")
        .then()
        .statusCode(200)
        .body("$.size()", is(1),
                "name", containsInAnyOrder(this.apple.getName()),
                "description", containsInAnyOrder(this.apple.getDescription()));
    }

}